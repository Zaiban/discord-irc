import Bot from './bot';

const COMMAND_CHAR = '!';

/*
  This is an extension to Bot class, allowing it to respond to commands and talk.
*/

class KoodiluolaBot extends Bot {
  respond(message) {
    let command;

    // If message starts with COMMAND_CHAR, parse it.
    if (typeof message === 'string' && message.charAt(0) === COMMAND_CHAR) {
      command = message.slice(1);
    } else if (message.content && message.content.charAt(0) === COMMAND_CHAR) {
      command = message.content.slice(1);
    }

    // If command is defined, set respond.
    if (command) {
      let respondMessage;

      // Add new commands to this switch
      switch (command) {
        case 'hello':
          respondMessage = 'Hey';
          break;
        default:
          break;
      }

      // If respond is defined, send it to both Discord and IRC.
      if (respondMessage) {
        this.ircClient.say(this.channels[0], respondMessage);
        const discordChannelName = this.invertedMapping[this.channels[0]];
        if (discordChannelName) {
      // #channel -> channel before retrieving and select only text channels:
          const discordChannel = this.discord.channels
        .filter(c => c.type === 'text')
        .find('name', discordChannelName.slice(1));
          discordChannel.sendMessage(respondMessage);
        }
      }
    }
  }

  /*
    Override sendToIRC and sendToDiscord
    Call the superclass method and pass message to this.respond
  */
  sendToIRC(message) {
    Bot.prototype.sendToIRC.apply(this, [message]);
    this.respond(message);
  }
  sendToDiscord(author, channel, text) {
    Bot.prototype.sendToDiscord.apply(this, [author, channel, text]);
    this.respond(text);
  }
}

export default KoodiluolaBot;
